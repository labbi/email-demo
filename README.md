# Email Demo

## Getting started
#### Step 1: Clone the repository
git clone https://gitlab.com/labbi/email-demo.git
#### Step 2: Running the server
Run 'mvn spring-boot:run'

# Technology decisions
## Database H2
The H2 is a very fast and low requirement database, which excels in demo or prototype projects. It circumvents the need
of defining an exterior database like postgres, mssql or mongo. Which includes extra work for setting up a docker compose file
and e.g. using 'Testcontainers' for integration tests. 

It would not be my preferred choice for any project outside a demo or prototype.

# Design
## Differentiation of DTO and Entity
Its good practice to differentiate between the domain entity and data-transfer-objects, due to following reasons.
Entity may implement behavior that can be applied to certain use cases. By splitting these objects you secure that no 
data access is possible outside defined services that build the bridge between the entity and the dto. Furthermore it 
separates the concerns of the objects e.g. storing the data and retrieving it via a rest api.
It comes with the drawback of having to implement mapper classes to convert the entity into the dto and vice versa.
