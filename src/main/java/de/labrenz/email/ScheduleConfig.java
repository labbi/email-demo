package de.labrenz.email;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@Configuration
public class ScheduleConfig {

    public static final String CLEAN_UP_CRON_EXPRESSION = "0 0 22 * * ?";
    public static final String SPAM_DETECTION_CRON_EXPRESSION = "0 0 10 * * ?";

}
