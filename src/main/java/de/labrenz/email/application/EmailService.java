package de.labrenz.email.application;

import de.labrenz.email.domain.Email;
import de.labrenz.email.domain.EmailRepository;
import de.labrenz.email.domain.EmailState;
import de.labrenz.email.domain.exceptions.EmailCanNotBeUpdatedException;
import de.labrenz.email.domain.exceptions.EmailNotFoundException;
import de.labrenz.email.web.EmailDto;
import de.labrenz.email.web.EmailDtos;
import de.labrenz.email.web.EmailMapper;
import de.labrenz.email.web.exceptions.MissingIdException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmailService {

    private final EmailRepository repository;
    private final EmailMapper mapper;

    public EmailDto readEmail(String id) {
        var savedEmail = findEmail(id);
        return mapper.toDto(savedEmail);
    }

    public EmailDtos readEmails(List<String> ids) {
        var savedEmails = findEmails(ids);
        return EmailDtos.builder()
                .emails(savedEmails.stream().map(mapper::toDto).toList())
                .build();
    }

    private List<Email> findEmails(List<String> ids) {
        if(ids.isEmpty())
            throw new MissingIdException();
        var savedEmails = repository.findAllById(ids);
        if(savedEmails.isEmpty())
            throw new EmailNotFoundException(ids.toString());
        return savedEmails;
    }

    private Email findEmail(String id) {
        if (id == null || id.isBlank())
            throw new MissingIdException();
        var savedEmail = repository.findById(id);
        if (savedEmail.isEmpty())
           throw new EmailNotFoundException(id);
        return savedEmail.get();
    }

    public EmailDto createEmail(Email email) {
        email.setCreated(Instant.now());
        email.setChanged(Instant.now());
        return mapper.toDto(repository.save(email));
    }

    public EmailDtos createEmails(List<Email> emails) {
        var createdEmails = repository.saveAll(emails.stream().map(email -> {
            email.setCreated(Instant.now());
            email.setChanged(Instant.now());
            return email;
        }).toList());

        return EmailDtos.builder()
                .emails(createdEmails.stream().map(mapper::toDto).toList())
                .build();
    }

    public EmailDto updateEmail(Email email) {
        var savedEmail = repository.findById(email.getId());
        if (savedEmail.isEmpty())
            throw new EmailNotFoundException(email.getId());

        if (!savedEmail.get().getState().equals(EmailState.DRAFT))
            throw new EmailCanNotBeUpdatedException(email.getId());
        email.setChanged(Instant.now());
        return mapper.toDto(repository.save(email));
    }

    public EmailDto deleteEmail(String id) {
        var email = findEmail(id);
        email.setState(EmailState.DELETED);
        return mapper.toDto(repository.save(email));
    }

    public EmailDtos deleteEmails(List<String> ids) {
        var emails = findEmails(ids);
        emails = emails.stream().map(email -> {
                    email.setState(EmailState.DELETED);
                    return email;
                }).toList();

        return EmailDtos.builder()
                .emails(repository.saveAll(emails).stream().map(mapper::toDto).toList())
                .build();
    }


}
