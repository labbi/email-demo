package de.labrenz.email.application;

import de.labrenz.email.domain.EmailRepository;
import de.labrenz.email.domain.EmailState;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static de.labrenz.email.ScheduleConfig.CLEAN_UP_CRON_EXPRESSION;
import static de.labrenz.email.ScheduleConfig.SPAM_DETECTION_CRON_EXPRESSION;

@Service
@RequiredArgsConstructor
public class ScheduleService {

    private static final String CARL = "carl@gbtec.com";

    private final EmailRepository repository;
    private final EmailService service;

    @Scheduled(cron = SPAM_DETECTION_CRON_EXPRESSION)
    public void spamDetection() {
        var spamEmails = repository.findEmailsByReceiversContainsOrSender(CARL, CARL);
        spamEmails.forEach(email -> {
            email.setState(EmailState.SPAM);
            service.updateEmail(email);
        });
    }

    @Scheduled(cron = CLEAN_UP_CRON_EXPRESSION)
    public void removeDeletedEmails() {
        repository.deleteEmailsByStateIs(EmailState.DELETED);
    }
}
