package de.labrenz.email.web;

import de.labrenz.email.application.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/email")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;
    private final EmailMapper mapper;

    @GetMapping(value = "/{id}")
    public ResponseEntity<EmailDto> findById(@PathVariable("id") String id) {
        return ResponseEntity.ok(emailService.readEmail(id));
    }

    @PutMapping
    public ResponseEntity<EmailDto> update(@RequestBody EmailDto body) {
        return ResponseEntity.ok(emailService.updateEmail(mapper.toEntity(body)));
    }

    @PostMapping
    public ResponseEntity<EmailDto> create(@RequestBody EmailDto body) {
        return ResponseEntity.ok(emailService.createEmail(mapper.toEntity(body)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        emailService.deleteEmail(id);
        return ResponseEntity.ok().build();
    }

}
