package de.labrenz.email.web;

import de.labrenz.email.application.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/email/bulk")
@RequiredArgsConstructor
public class EmailBulkController {

    private final EmailService emailService;
    private final EmailMapper mapper;

    @PostMapping("/read")
    public ResponseEntity<EmailDtos> findByIds(@RequestBody List<String> ids) {
        return ResponseEntity.ok(emailService.readEmails(ids));
    }

    @PostMapping("/create")
    public ResponseEntity<EmailDtos> createBulk(@RequestBody EmailDtos body) {
        return ResponseEntity.ok(emailService.createEmails(body.getEmails().stream().map(mapper::toEntity).toList()));
    }

    @PostMapping("/delete")
    public ResponseEntity<EmailDtos> deleteBulk(@RequestBody List<String> ids) {
        return ResponseEntity.ok(emailService.deleteEmails(ids));
    }
}
