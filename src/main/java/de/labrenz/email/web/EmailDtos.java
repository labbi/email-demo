package de.labrenz.email.web;


import lombok.*;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailDtos {
    @Singular
    private List<EmailDto> emails;
}
