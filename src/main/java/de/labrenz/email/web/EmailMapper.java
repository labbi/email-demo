package de.labrenz.email.web;

import de.labrenz.email.domain.Email;
import de.labrenz.email.domain.EmailState;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class EmailMapper {

    public EmailDto toDto(Email email) {
        return EmailDto.builder()
                .id(email.getId())
                .from(email.getSender())
                .to(email.getReceivers())
                .copies(email.getCopies())
                .subject(email.getSubject())
                .content(email.getContent())
                .state(email.getState())
                .build();
    }

    public Email toEntity(EmailDto emailDto) {
        return Email.builder()
                .id(emailDto.getId())
                .receivers(emailDto.getTo())
                .sender(emailDto.getFrom())
                .content(emailDto.getContent())
                .copies(emailDto.getCopies() == null ? new ArrayList<>() : emailDto.getCopies())
                .subject(emailDto.getSubject())
                .state(emailDto.getState() == null ? EmailState.DRAFT : emailDto.getState())
                .build();
    }
}
