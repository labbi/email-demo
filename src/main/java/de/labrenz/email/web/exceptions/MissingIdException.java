package de.labrenz.email.web.exceptions;

public class MissingIdException extends RuntimeException{

    public MissingIdException() {
        super("No id was provided");
    }
}
