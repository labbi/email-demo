package de.labrenz.email.web;

import de.labrenz.email.domain.exceptions.EmailCanNotBeUpdatedException;
import de.labrenz.email.domain.exceptions.EmailNotFoundException;
import de.labrenz.email.web.exceptions.MissingIdException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String CONFLICT = "The requested operation resulted in a conflict.";
    public static final String NOT_FOUND = "The requested resource was not found.";
    public static final String BAD_REQUEST = "An error occurred while performing your action. Please inform our support.";

    @ExceptionHandler(value = {EmailNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, NOT_FOUND, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = {EmailCanNotBeUpdatedException.class, MissingIdException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, CONFLICT, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> defaultExceptionHandler(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, BAD_REQUEST, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
