package de.labrenz.email.web;

import de.labrenz.email.domain.EmailState;
import lombok.*;

import java.util.List;

@Builder
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class EmailDto {

    private String id;
    private String from;
    private List<String> to;
    private List<String> copies;
    private String subject;
    private String content;
    private EmailState state;

}
