package de.labrenz.email.domain;

public enum EmailState {
    DRAFT,SENT,SPAM,DELETED
}
