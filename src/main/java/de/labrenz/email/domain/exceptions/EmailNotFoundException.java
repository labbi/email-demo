package de.labrenz.email.domain.exceptions;

public class EmailNotFoundException extends RuntimeException{

    public EmailNotFoundException(String id) {
        super("Email for " + id + " could not be found");
    }
}
