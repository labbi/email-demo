package de.labrenz.email.domain.exceptions;

public class EmailCanNotBeUpdatedException extends RuntimeException {

    public EmailCanNotBeUpdatedException(String id) {
        super("Email with id " + id +" is not in state draft and can not be updated");
    }

}
