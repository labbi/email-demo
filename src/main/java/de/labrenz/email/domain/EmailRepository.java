package de.labrenz.email.domain;

import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface EmailRepository extends ListCrudRepository<Email, String> {

    Stream<Email> findEmailsByReceiversContainsOrSender(String receiver, String sender);
    void deleteEmailsByStateIs(EmailState state);
}
