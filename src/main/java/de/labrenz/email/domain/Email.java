package de.labrenz.email.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UuidGenerator;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    @Id
    @UuidGenerator
    private String id;
    private String sender;
    @ElementCollection
    private List<String> receivers;
    @ElementCollection
    private List<String> copies;
    private String subject;
    private String content;
    private EmailState state;
    private Instant created;
    private Instant changed;

}
