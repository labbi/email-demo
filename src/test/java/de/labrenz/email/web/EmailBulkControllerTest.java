package de.labrenz.email.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.labrenz.email.application.EmailService;
import de.labrenz.email.domain.EmailState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = {EmailBulkController.class, EmailMapper.class}
)
@AutoConfigureMockMvc
@EnableWebMvc
class EmailBulkControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @MockBean
    private EmailService service;

    @Autowired
    private EmailMapper mapper;

    @BeforeEach
    public void setup() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void createEmailInBulkReturnsEmails() throws Exception {
        var dto1 = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        var dto2 = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        var dto = EmailDtos.builder().email(dto1).email(dto2).build();

        when(service.createEmails(any())).thenReturn(dto);

        String expectedJson = objectMapper.writeValueAsString(dto);

        mockMvc.perform(post("/email/bulk/create")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedJson))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void readEmailInBulkReturnsEmails() throws Exception {
        var dto1 = EmailDto.builder()
                .id("id1")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        var dto2 = EmailDto.builder()
                .id("id2")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        var dto = EmailDtos.builder().email(dto1).email(dto2).build();

        when(service.readEmails(any())).thenReturn(dto);

        String input = objectMapper.writeValueAsString(List.of(dto1.getId(), dto2.getId()));
        String expectedJson = objectMapper.writeValueAsString(dto);

        mockMvc.perform(post("/email/bulk/read")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void deleteEmailInBulkReturnsEmails() throws Exception {
        var dto1 = EmailDto.builder()
                .id("id1")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DELETED)
                .build();

        var dto2 = EmailDto.builder()
                .id("id2")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DELETED)
                .build();

        var dto = EmailDtos.builder().email(dto1).email(dto2).build();
        when(service.deleteEmails(any())).thenReturn(dto);

        String input = objectMapper.writeValueAsString(List.of("id", "id2"));

        mockMvc.perform(post("/email/bulk/delete")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input))
                .andExpect(status().isOk());
    }

}