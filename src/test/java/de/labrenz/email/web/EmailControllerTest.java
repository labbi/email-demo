package de.labrenz.email.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.labrenz.email.application.EmailService;
import de.labrenz.email.domain.EmailState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = {EmailController.class, EmailMapper.class}
)
@AutoConfigureMockMvc
@EnableWebMvc
class EmailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @MockBean
    private EmailService service;

    @BeforeEach
    public void setup() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void createEmailReturnsEmail() throws Exception {
        var dto = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        when(service.createEmail(any())).thenReturn(dto);

        String expectedJson = objectMapper.writeValueAsString(dto);

        mockMvc.perform(post("/email")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedJson))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void readEmailReturnsEmail() throws Exception {
        var dto = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        when(service.readEmail("id")).thenReturn(dto);

        String expectedJson = objectMapper.writeValueAsString(dto);

        mockMvc.perform(get("/email/id").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void updateEmailReturnsEmail() throws Exception {
        var dto = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DRAFT)
                .build();

        when(service.updateEmail(any())).thenReturn(dto);

        String expectedJson = objectMapper.writeValueAsString(dto);

        mockMvc.perform(put("/email")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedJson))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    void deleteEmailReturnsEmail() throws Exception {
        var dto = EmailDto.builder()
                .id("id")
                .from("mustermann@muster.de")
                .to(List.of("musterfrau@muster.de"))
                .subject("Test email")
                .content("Test content")
                .copies(List.of("musterCC@Muster.de"))
                .state(EmailState.DELETED)
                .build();
        when(service.deleteEmail("id")).thenReturn(dto);

        mockMvc.perform(delete("/email/id").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}