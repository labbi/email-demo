package de.labrenz.email;

import de.labrenz.email.domain.EmailState;
import de.labrenz.email.web.EmailDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmailControllerIntegrationTest {

    public static final String EMAIL1 = "mustermann@muster.de";
    public static final String EMAIL2 = "musterfrau@muster.de";
    public static final String SENDER = "application@gbtec.de";
    public static final String SUBJECT = "Application test";
    public static final String CONTENT = "This is an application test";
    @Autowired
    private WebTestClient webTestClient;

    @Test
    void emailProcess_Create_Read_Update_Delete() {
        var emailResponse = webTestClient
                .post()
                .uri("/email")
                .body(BodyInserters.fromValue(EmailDto.builder()
                                .to(List.of(EMAIL1, EMAIL2))
                                .from(SENDER)
                                .subject(SUBJECT)
                                .content(CONTENT)
                        .build()))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDto.class);

        var email = emailResponse.getResponseBody().blockFirst();

        assertThat(email.getId()).isNotNull().isNotBlank();
        assertThat(email.getTo()).containsExactlyInAnyOrder(EMAIL2, EMAIL1);
        assertThat(email.getFrom()).isEqualTo(SENDER);
        assertThat(email.getState()).isEqualTo(EmailState.DRAFT);
        assertThat(email.getCopies()).isEmpty();
        assertThat(email.getSubject()).isEqualTo(SUBJECT);
        assertThat(email.getContent()).isEqualTo(CONTENT);

        emailResponse = webTestClient
                .get()
                .uri("/email/" + email.getId())
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDto.class);

        email = emailResponse.getResponseBody().blockFirst();

        assertThat(email.getId()).isNotNull().isNotBlank();
        assertThat(email.getTo()).containsExactlyInAnyOrder(EMAIL2, EMAIL1);
        assertThat(email.getFrom()).isEqualTo(SENDER);
        assertThat(email.getState()).isEqualTo(EmailState.DRAFT);
        assertThat(email.getCopies()).isEmpty();
        assertThat(email.getSubject()).isEqualTo(SUBJECT);
        assertThat(email.getContent()).isEqualTo(CONTENT);

        emailResponse = webTestClient
                .put()
                .uri("/email")
                .body(BodyInserters.fromValue(EmailDto.builder()
                        .id(email.getId())
                        .to(List.of(EMAIL1, EMAIL2))
                        .from(SENDER)
                        .subject(SUBJECT)
                        .content(CONTENT)
                        .state(EmailState.SENT)
                        .build()))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDto.class);

        email = emailResponse.getResponseBody().blockFirst();

        assertThat(email.getId()).isNotNull().isNotBlank();
        assertThat(email.getTo()).containsExactlyInAnyOrder(EMAIL2, EMAIL1);
        assertThat(email.getFrom()).isEqualTo(SENDER);
        assertThat(email.getState()).isEqualTo(EmailState.SENT);
        assertThat(email.getCopies()).isEmpty();
        assertThat(email.getSubject()).isEqualTo(SUBJECT);
        assertThat(email.getContent()).isEqualTo(CONTENT);

        webTestClient
                .delete()
                .uri("/email/" + email.getId())
                .exchange()
                .expectStatus().is2xxSuccessful();

        emailResponse = webTestClient
                .get()
                .uri("/email/" + email.getId())
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDto.class);

        email = emailResponse.getResponseBody().blockFirst();
        assertThat(email.getState()).isEqualTo(EmailState.DELETED);
    }
}
