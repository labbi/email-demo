package de.labrenz.email.application;

import de.labrenz.email.domain.Email;
import de.labrenz.email.domain.EmailRepository;
import de.labrenz.email.domain.EmailState;
import de.labrenz.email.domain.exceptions.EmailCanNotBeUpdatedException;
import de.labrenz.email.domain.exceptions.EmailNotFoundException;
import de.labrenz.email.web.EmailMapper;
import de.labrenz.email.web.exceptions.MissingIdException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private EmailRepository emailRepository;

    private EmailService sut;

    private EmailMapper mapper;
    @BeforeEach
    public void setup() {
        mapper = new EmailMapper();
        sut = new EmailService(emailRepository, mapper);
    }
    @Test
    void createEmailReturnsDto() {
        var email = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("content")
                .subject("subject")
                .build();

        when(emailRepository.save(any())).thenAnswer(inv -> inv.getArgument(0));

        var result = sut.createEmail(email);

        assertThat(result.getId()).isNotNull().isNotBlank();
        assertThat(result.getFrom()).isEqualTo(email.getSender());
        assertThat(result.getTo()).isEqualTo(email.getReceivers());
        assertThat(result.getContent()).isEqualTo(email.getContent());
        assertThat(result.getSubject()).isEqualTo(email.getSubject());
    }

    @Test
    void readEmailReturnsDto() {
        var email = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("content")
                .subject("subject")
                .build();

        when(emailRepository.findById("id")).thenReturn(Optional.of(email));

        assertThat(sut.readEmail("id")).isEqualTo(mapper.toDto(email));
    }

    @Test
    void readEmailReturnsNotFoundExceptionIfEmailWithIdWasNotFound() {
        when(emailRepository.findById("id")).thenReturn(Optional.empty());

        assertThrows(EmailNotFoundException.class, () -> sut.readEmail("id"));
    }

    @Test
    void readEmailReturnsMissingIdExceptionIfNullWasGiven() {
        assertThrows(MissingIdException.class, () -> sut.readEmail(null));
    }

    @Test
    void readEmailReturnsMissingIdExceptionIfBlankStringWasGiven() {
        assertThrows(MissingIdException.class, () -> sut.readEmail("   "));
    }

    @Test
    void updateEmailReturnsDto() {
        var oldEmail = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("old content")
                .subject("old subject")
                .state(EmailState.DRAFT)
                .build();

        var email = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("content")
                .subject("subject")
                .state(EmailState.SENT)
                .build();

        when(emailRepository.findById("id")).thenReturn(Optional.of(oldEmail));
        when(emailRepository.save(any())).thenAnswer(inv -> inv.getArgument(0));

        assertThat(sut.updateEmail(email)).isEqualTo(mapper.toDto(email));
    }

    @Test
    void updateEmailReturnsNotFoundExceptionIfEmailWithIdWasNotFound() {
        var email = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("content")
                .subject("subject")
                .state(EmailState.DRAFT)
                .build();
        when(emailRepository.findById("id")).thenReturn(Optional.empty());

        assertThrows(EmailNotFoundException.class, () -> sut.updateEmail(email));
    }

    @Test
    void updateEmailReturnsCanNotBeUpdatedExceptionIfEmailIsNotInStateDraft() {
        var email = Email.builder()
                .id("id")
                .sender("mustermann@muster.de")
                .receivers(List.of("musterfrau@muster.de"))
                .content("content")
                .subject("subject")
                .state(EmailState.SENT)
                .build();
        when(emailRepository.findById("id")).thenReturn(Optional.of(email));

        assertThrows(EmailCanNotBeUpdatedException.class, () -> sut.updateEmail(email));
    }

    @Test
    void deleteEmailRemovesEntryInRepository() {
        var email = Email.builder()
                .id("id")
                .state(EmailState.DELETED)
                .build();
        when(emailRepository.findById("id")).thenReturn(Optional.of(email));
        when(emailRepository.save(any())).thenReturn(email);

        assertDoesNotThrow(() -> sut.deleteEmail("id"));
    }
}