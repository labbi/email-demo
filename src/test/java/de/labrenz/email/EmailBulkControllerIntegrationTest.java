package de.labrenz.email;

import de.labrenz.email.domain.EmailState;
import de.labrenz.email.web.EmailDto;
import de.labrenz.email.web.EmailDtos;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmailBulkControllerIntegrationTest {

    public static final String EMAIL1 = "mustermann@muster.de";
    public static final String EMAIL2 = "musterfrau@muster.de";
    public static final String SENDER = "application@gbtec.de";
    public static final String SUBJECT = "Application test";
    public static final String CONTENT = "This is an application test";
    @Autowired
    private WebTestClient webTestClient;

    @Test
    void emailBulkProcess_Create_Read_Delete() {
        var emailDto1 = EmailDto.builder()
                .to(List.of(EMAIL1))
                .from(SENDER)
                .subject(SUBJECT)
                .content(CONTENT)
                .build();
        var emailDto2 = EmailDto.builder()
                .to(List.of(EMAIL2))
                .from(SENDER)
                .subject(SUBJECT)
                .content(CONTENT)
                .build();
        var emailResponse = webTestClient
                .post()
                .uri("/email/bulk/create")
                .body(BodyInserters.fromValue(EmailDtos.builder()
                                .email(emailDto1)
                                .email(emailDto2)
                                .build()))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDtos.class);

        var emails = emailResponse.getResponseBody().blockFirst();

        emails.getEmails().forEach(emailDto -> {
            assertThat(emailDto.getId()).isNotNull().isNotBlank();
            assertThat(emailDto.getFrom()).isEqualTo(SENDER);
            assertThat(emailDto.getState()).isEqualTo(EmailState.DRAFT);
            assertThat(emailDto.getCopies()).isEmpty();
            assertThat(emailDto.getSubject()).isEqualTo(SUBJECT);
            assertThat(emailDto.getContent()).isEqualTo(CONTENT);
            if (emailDto.getTo().contains(EMAIL1))
                assertThat(emailDto.getTo()).containsExactly(EMAIL1);
            else if (emailDto.getTo().contains(EMAIL2))
                assertThat(emailDto.getTo()).containsExactly(EMAIL2);
        });

        var ids = emails.getEmails().stream().map(EmailDto::getId).toList();
        emailResponse = webTestClient
                .post()
                .uri("/email/bulk/read")
                .body(BodyInserters.fromValue(ids))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDtos.class);

        emails = emailResponse.getResponseBody().blockFirst();
        emails.getEmails().forEach(emailDto -> {
            assertThat(emailDto.getId()).isNotNull().isNotBlank();
            assertThat(emailDto.getFrom()).isEqualTo(SENDER);
            assertThat(emailDto.getState()).isEqualTo(EmailState.DRAFT);
            assertThat(emailDto.getCopies()).isEmpty();
            assertThat(emailDto.getSubject()).isEqualTo(SUBJECT);
            assertThat(emailDto.getContent()).isEqualTo(CONTENT);
            if (emailDto.getTo().contains(EMAIL1))
                assertThat(emailDto.getTo()).containsExactly(EMAIL1);
            else if (emailDto.getTo().contains(EMAIL2))
                assertThat(emailDto.getTo()).containsExactly(EMAIL2);
        });

        webTestClient
                .post()
                .uri("/email/bulk/delete")
                .body(BodyInserters.fromValue(ids))
                .exchange()
                .expectStatus().is2xxSuccessful();

        emailResponse = webTestClient
                .post()
                .uri("/email/bulk/read")
                .body(BodyInserters.fromValue(ids))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .returnResult(EmailDtos.class);

        emails = emailResponse.getResponseBody().blockFirst();
        emails.getEmails().forEach(emailDto -> {
            assertThat(emailDto.getState()).isEqualTo(EmailState.DELETED);
        });
    }
}
